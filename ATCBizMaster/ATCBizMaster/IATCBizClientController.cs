﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ATCBizMaster
{
    [ServiceContract(CallbackContract=typeof(IATCBizClientCallback))]
    public interface IATCBizClientController
    {
        [OperationContract]
        Dictionary<int, Airport> GetAirports();

        [OperationContract]
        Airport GetAirport(int apID);

        [OperationContract(IsOneWay=true)]
        void PerformTimeTick(int minutes);

        [OperationContract]
        int InitialConnect();

        [OperationContract]
        void Disconnect();
    }

    [ServiceContract]
    public interface IATCBizClientCallback
    {
        [OperationContract(IsOneWay=true)]
        void UpdateAirports(List<Airport> airports);
    }
}
