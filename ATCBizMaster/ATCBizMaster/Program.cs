﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCDatabase;
using System.ServiceModel;

namespace ATCBizMaster
{
    public class HostLocations
    {
        public const String ATCMasterURL = "net.tcp://localhost:50000/ATCBizMaster";
        public const String ATCClientURL = "net.tcp://localhost:50000/ATCBizClientMaster";
    }


    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost masterHost, clientHost;
            NetTcpBinding masterTcpBinding = new NetTcpBinding();
            NetTcpBinding clientTcpBinding = new NetTcpBinding();

            ATCBizSlaveControllerImpl master = new ATCBizSlaveControllerImpl();
            ATCBizClientControllerImpl client = new ATCBizClientControllerImpl(master);

            masterTcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
            masterTcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;
            masterTcpBinding.OpenTimeout = new TimeSpan(1, 0, 0);
            masterTcpBinding.CloseTimeout = new TimeSpan(1, 0, 0);

            clientTcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
            clientTcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;
            clientTcpBinding.OpenTimeout = new TimeSpan(1, 0, 0);
            clientTcpBinding.CloseTimeout = new TimeSpan(1, 0, 0);

            masterHost = new ServiceHost(master);
            masterHost.AddServiceEndpoint(typeof(IATCBizSlaveController), masterTcpBinding, HostLocations.ATCMasterURL);

            clientHost = new ServiceHost(client);
            clientHost.AddServiceEndpoint(typeof(IATCBizClientController), clientTcpBinding, HostLocations.ATCClientURL);

            try
            {
                masterHost.Open();
                clientHost.Open();

                Console.WriteLine("Press Enter to exit.");
                Console.ReadLine();

                masterHost.Close();
                clientHost.Close();
            }
            catch (AddressAlreadyInUseException e)
            {
                Console.WriteLine("A copy of the server is already runnine.");
                Console.WriteLine("Exiting.");
            }
            // Only use in production.
            /*catch (Exception e)
            {
                Console.WriteLine("Fatal Error encountered.");
                Console.WriteLine("Exiting.");
            }*/
        }
    }
}
