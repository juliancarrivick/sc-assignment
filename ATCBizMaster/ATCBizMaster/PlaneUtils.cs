﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace ATCBizMaster
{
    /// <summary>
    /// Provide simple utilities to clients.
    /// </summary>
    public class PlaneUtils
    {
        /// <summary>
        /// Given a list of planes, find the one which has been landed the longest.
        /// </summary>
        /// <param name="planes">A list of planes</param>
        /// <returns>The plane with the highest landed time.</returns>
        public static Plane LongestLandedPlane(List<Plane> planes)
        {
            Plane longestLandedPlane = null;

            foreach (Plane p in planes)
            {
                if (longestLandedPlane == null)
                {
                    longestLandedPlane = p;
                }
                else if (p.TimeLanded < longestLandedPlane.TimeLanded)
                {
                    longestLandedPlane = p;
                }
            }

            return longestLandedPlane;
        }

        /// <summary>
        /// Returns a list of planes that are ready to take off.
        /// </summary>
        /// <param name="planes">The list of planes to check</param>
        /// <returns>The list of planes ready to take off.</returns>
        public static List<Plane> GetPlanesReadyForTakeOff(List<Plane> planes)
        {
            List<Plane> landedPlanes = GetPlanesInState(planes, PlaneState.Landed);
            List<Plane> planesLandedOver60Min = new List<Plane>();

            foreach (Plane p in landedPlanes)
            {
                if (p.TimeLanded >= 60)
                {
                    planesLandedOver60Min.Add(p);
                }
            }

            return planesLandedOver60Min;
        }

        /// <summary>
        /// Returns the list of planes in the specified state
        /// </summary>
        /// <param name="planes">The planes to check</param>
        /// <param name="state">The state to check for</param>
        /// <returns>The planes in the specified state.</returns>
        public static List<Plane> GetPlanesInState(List<Plane> planes, PlaneState state)
        {
            List<Plane> statePlanes = new List<Plane>();

            foreach (Plane p in planes)
            {
                if (p.State == state)
                {
                    statePlanes.Add(p);
                }
            }

            return statePlanes;
        }

        /// <summary>
        /// Update a list of planes fuel levels.
        /// </summary>
        /// <param name="planes">Planes to update</param>
        /// <param name="min">The number of minutes elapsed.</param>
        public static void UpdatePlaneFuelLevels(List<Plane> planes, int min)
        {
            foreach (Plane p in planes)
            {
                UpdatePlaneFuelLevels(p, min);
            }
        }
        
        /// <summary>
        /// Update a single plane's fuel levels. 
        /// </summary>
        /// <param name="plane">The plane to update</param>
        /// <param name="min">The number of minutes that have elapsed.</param>
        public static void UpdatePlaneFuelLevels(Plane plane, int min)
        {
            // Don't strictly need to check if not landed, but better safe than sorry.
            if (plane.State != PlaneState.Landed)
            {
                plane.FuelLitres -= plane.FuelConsPerHour * (double)min / 60.0;

                if (plane.FuelLitres <= 0.0)
                {
                    plane.FuelLitres = 0.0;
                    plane.State = PlaneState.Crashed;
                    Console.WriteLine("*** PlaneID: " + plane.ID + " CRASHED!!! ***");
                }
            }
        }

        /// <summary>
        /// Finds the plane with the lowest fuel levels.
        /// </summary>
        /// <param name="planes">Planes to check</param>
        /// <returns>The plane with the lowest fuel level.</returns>
        public static Plane GetPlaneWithLowestFuel(List<Plane> planes)
        {
            Plane planeLowestFuel = null;

            foreach (Plane p in planes)
            {
                if (planeLowestFuel == null)
                {
                    planeLowestFuel = p;
                }
                else if (p.FuelLitres < planeLowestFuel.FuelLitres)
                {
                    planeLowestFuel = p;
                }
            }

            return planeLowestFuel;
        }

        public static void AddTimeToLandedPlanes(List<Plane> planes, int min)
        {
            List<Plane> landedPlanes = PlaneUtils.GetPlanesInState(planes, PlaneState.Landed);

            foreach (Plane p in landedPlanes)
            {
                p.TimeLanded += min;
            }
        }

        /// <summary>
        /// Add the distance travelled in the time tick to the list of planes.
        /// </summary>
        /// <param name="planes">The planes to update</param>
        /// <param name="min">The time that has elapsed.</param>
        public static void AddTravelDistance(IATCBizSlaveController master, List<Plane> planes, int min)
        {
            double timeTravelledInTick, routeDistance;

            foreach (Plane p in planes)
            {
                timeTravelledInTick = (double)min / 60.0 * p.CruisingKPH;
                routeDistance = master.GetAirRoute(p.CurrentAirRouteID).DistanceKM;

                p.DistanceAlongRoute += timeTravelledInTick;

                if (p.DistanceAlongRoute > routeDistance)
                {
                    p.DistanceAlongRoute = routeDistance;
                    p.State = PlaneState.Circling;
                }
            }
        }

        /// <summary>
        /// Choose between two planes to land
        /// </summary>
        /// <param name="circlingLowestFuel">The circling plane</param>
        /// <param name="enteringCirclingLowestFuel">The entering circling plane</param>
        /// <param name="min">The number of minutes that have passed</param>
        /// <returns>The plane to land/</returns>
        public static Plane ChoosePlaneToLand(IATCBizSlaveController master, Plane circlingLowestFuel, Plane enteringCirclingLowestFuel, int min)
        {
            Plane planeToLand = null;

            if (enteringCirclingLowestFuel.FuelLitres < circlingLowestFuel.FuelLitres
             && CanEnteringCirclingPlaneReachAirport(master, enteringCirclingLowestFuel, min))
            {
                planeToLand = enteringCirclingLowestFuel;
            }
            else
            {
                planeToLand = circlingLowestFuel;
            }

            return planeToLand;
        }

        /// <summary>
        /// Checks if a plane that is entering circling can reach the airport in the 
        /// next tick of time.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="min"></param>
        /// <returns></returns>
        public static bool CanEnteringCirclingPlaneReachAirport(IATCBizSlaveController master, Plane p, int min)
        {
            double timeTravelledInTick = (double)min / 60.0 * p.CruisingKPH;
            double airrouteDistance = master.GetAirRoute(p.CurrentAirRouteID).DistanceKM;
            double distanceToAirport = airrouteDistance - p.DistanceAlongRoute;

            return timeTravelledInTick > distanceToAirport;
        }
    }
}
