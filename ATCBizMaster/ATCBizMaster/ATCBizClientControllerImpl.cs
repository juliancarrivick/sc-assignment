﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.CompilerServices;

namespace ATCBizMaster
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class ATCBizClientControllerImpl : IATCBizClientController
    {
        private ATCBizSlaveControllerImpl m_masterController;
        private ISet<IATCBizClientCallback> m_clientCallbacks;

        /// <summary>
        /// Initliase the Client Controller.
        /// </summary>
        /// <param name="master">A copy of the MasterSlave Controller Object</param>
        public ATCBizClientControllerImpl(ATCBizSlaveControllerImpl master)
        {
            m_masterController = master;
            m_clientCallbacks = new HashSet<IATCBizClientCallback>();
        }

        /// <summary>
        /// Gets the most up to date airports from the MasterController.
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, Airport> GetAirports()
        {
            return m_masterController.getCurrentAirports();
        }

        /// <summary>
        /// Get an airport's data from its ID
        /// </summary>
        /// <param name="apID">The airport ID</param>
        /// <returns>The airport data</returns>
        public Airport GetAirport(int apID)
        {
            return m_masterController.GetAirport(apID);
        }

        /// <summary>
        /// Ticks the given amount of time.
        /// </summary>
        /// <param name="minutes">The number of minutes to tick.</param>
        public void PerformTimeTick(int minutes)
        {
            m_masterController.PerformTimeTick(minutes);

            List<Airport> updatedAirports = this.GetAirports().Values.ToList();

            foreach (IATCBizClientCallback cb in m_clientCallbacks)
            {
                try
                {
                    cb.UpdateAirports(updatedAirports);
                }
                catch (CommunicationException ex)
                {
                    // If callback invalid, remove from list
                    m_clientCallbacks.Remove(cb);
                }
            }
        }

        /// <summary>
        /// Add the callback to the set of callbacks.
        /// </summary>
        public int InitialConnect()
        {
            m_clientCallbacks.Add(OperationContext.Current.GetCallbackChannel<IATCBizClientCallback>());

            return m_masterController.GetTime();
        }

        /// <summary>
        /// Remove the callback from the set of callbacks.
        /// </summary>
        public void Disconnect()
        {
            m_clientCallbacks.Remove(OperationContext.Current.GetCallbackChannel<IATCBizClientCallback>());
        }
    }
}
