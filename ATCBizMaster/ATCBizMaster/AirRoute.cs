﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCBizMaster
{
    [DataContract]
    public class AirRoute
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int FromAirportID { get; set; }
        [DataMember]
        public int ToAirportID { get; set; }
        [DataMember]
        public double DistanceKM { get; set; }

        public AirRoute(int ID, int FromAirportID, int ToAirportID, double distance)
        {
            this.ID = ID;
            this.FromAirportID = FromAirportID;
            this.ToAirportID = ToAirportID;
            this.DistanceKM = distance;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in this.GetType().GetProperties())
            {
                sb.Append(property.Name + ": " + property.GetValue(this, null) + ", ");
            }

            return sb.ToString();

        }
    }
}
