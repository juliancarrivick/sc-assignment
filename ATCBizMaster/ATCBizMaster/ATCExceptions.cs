﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCBizMaster
{
    public class NoAvailableAirportsException : Exception, ISerializable
    {
        public NoAvailableAirportsException()
            : base()
        { }

        public NoAvailableAirportsException(String msg)
            : base(msg)
        { }

        public NoAvailableAirportsException(String msg, Exception inner)
            : base(msg, inner)
        { }

        protected NoAvailableAirportsException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        { }
    }
}
