﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCBizMaster
{
    public enum PlaneState { Landed, InTransit, EnteringCircling, Circling, Crashed };

    [DataContract]
    public class Plane
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public String Type { get; set; }
        [DataMember]
        public PlaneState State { get; set; }
        [DataMember]
        public double CruisingKPH { get; set; }
        [DataMember]
        public double FuelConsPerHour { get; set; }
        [DataMember]
        public int CurrentAirRouteID { get; set; }
        [DataMember]
        public double DistanceAlongRoute { get; set; }
        [DataMember]
        public int CurrentAirportID { get; set; }
        [DataMember]
        public int TimeLanded { get; set; }
        [DataMember]
        public double FuelLitres { get; set; }

        public Plane(int ID, String planeType, double cruisingKPH, double fuelConsPerHour, int currAirport)
        {
            this.ID = ID;
            this.Type = planeType;
            this.State = PlaneState.Landed;
            this.CruisingKPH = cruisingKPH;
            this.FuelConsPerHour = fuelConsPerHour;
            this.CurrentAirRouteID = -1;
            this.DistanceAlongRoute = 0.0;
            this.CurrentAirportID = currAirport;
            this.TimeLanded = 0;
            this.FuelLitres = 0.0;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in this.GetType().GetProperties())
            {
                sb.Append(property.Name + ": " + property.GetValue(this, null) + ", ");
            }

            return sb.ToString();

        }
    }
}
