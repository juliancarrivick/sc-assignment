﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace ATCBizMaster
{
    [ServiceContract(CallbackContract=typeof(IATCBizSlaveControllerCallBack))]
    public interface IATCBizSlaveController
    {
        [OperationContract]
        void HandOverPlane(Plane plane, int AirportID);

        [OperationContract]
        Airport AirportInitialConnect();

        [OperationContract(IsOneWay=true)]
        void DisconnectAirport(Airport a);

        [OperationContract]
        void PerformTimeTick(int minutes);

        [OperationContract]
        AirRoute GetAirRoute(int routeID);
    }

    [ServiceContract]
    public interface IATCBizSlaveControllerCallBack
    {
        [OperationContract(IsOneWay = true)]
        void TimeTick(int minutes);

        [OperationContract]
        void TransferPlane(Plane plane);

        [OperationContract]
        Airport GetThisAirport();
    }
}
