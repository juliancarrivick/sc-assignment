﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace ATCBizMaster
{
    [DataContract]
    public class Airport
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public String Name { get; set; }
        [DataMember]
        public Dictionary<int, Plane> Planes { get; set; }
        [DataMember]
        public Dictionary<int, AirRoute> DepartingAirRoutes { get; set; }

        public Airport(int ID, String name)
        {
            this.ID = ID;
            this.Name = name;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in this.GetType().GetProperties())
            {
                sb.Append(property.Name + ": " + property.GetValue(this, null) + ", ");
            }

            return sb.ToString();

        }
    }
}
