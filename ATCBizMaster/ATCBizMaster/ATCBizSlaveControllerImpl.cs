﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using ATCDatabase;
using System.Runtime.CompilerServices;

namespace ATCBizMaster
{
    /// <summary>
    /// The implementation of the master controller. Is a singleton.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class ATCBizSlaveControllerImpl : IATCBizSlaveController, IDisposable
    {
        private Dictionary<int, IATCBizSlaveControllerCallBack> m_airportCallBacks;
        private Dictionary<int, Airport> m_airports;
        private int m_time;

        private delegate Airport AirportDelegate();
        private delegate void TimeTickDelegate(int min);

        /// <summary>
        /// Initialise the Master Server
        /// </summary>
        public ATCBizSlaveControllerImpl()
        {
            Console.WriteLine("Master Server Initialised");
            Console.WriteLine("Loading Database Data");
            loadDatabase();
            Console.WriteLine("...Done");

            m_airportCallBacks = new Dictionary<int, IATCBizSlaveControllerCallBack>();
            foreach (Airport ap in m_airports.Values)
            {
                m_airportCallBacks[ap.ID] = null;
            }

            m_time = 0;
        }

        /// <summary>
        /// Code to run when the object is disposed.
        /// </summary>
        public void Dispose()
        {
            Console.WriteLine("Master Server going down.");
        }

        /// <summary>
        /// Gets the most up to date airport data from each of the ATC Slaves
        /// </summary>
        /// <returns>The current airport states.</returns>
        public Dictionary<int, Airport> getCurrentAirports()
        {
            Dictionary<AirportDelegate, IAsyncResult> delegateToCallback = new Dictionary<AirportDelegate, IAsyncResult>();
            AirportDelegate tempDelegate;

            Dictionary<int, Airport> airports = new Dictionary<int, Airport>();
            Airport airport;

            // For each callback object, run asyncronously, mapping the delegate 
            // for that particular callback object to the AsyncResult Object that 
            // is returned by BeginInvoke.
            foreach (IATCBizSlaveControllerCallBack cb in m_airportCallBacks.Values)
            {
                // Since each Callback Object is a proxy, the delegate will not be
                // static over all the callbacks. Hence the need to set and it each 
                // time and to keep track of it.
                tempDelegate = cb.GetThisAirport;

                delegateToCallback[tempDelegate] = tempDelegate.BeginInvoke(null, null);
            }

            // For each entry in the dictionary, retrieve the result and store 
            // in the list of airports (techincally a dictionary).
            // Exceptions that happen in the threads will be thrown when EndInvoke
            // is run, so also need to check that.
            foreach (KeyValuePair<AirportDelegate, IAsyncResult> delToCB in delegateToCallback)
            {
                airport = delToCB.Key.EndInvoke(delToCB.Value);
                airports[airport.ID] = airport;
            }

            return airports;
        }

        /// <summary>
        /// Get the current server time in the simulation
        /// </summary>
        /// <returns>Time in minutes</returns>
        public int GetTime()
        {
            return m_time;
        }

        /// <summary>
        /// Get all data associated with the specified Airport.
        /// </summary>
        /// <param name="apID">The airport ID.</param>
        /// <returns>The Airport data.</returns>
        public Airport GetAirport(int apID)
        {
            return m_airportCallBacks[apID].GetThisAirport();
        }


        /********** INTERFACE METHODS **********/
        /* These methods are exposed to the outside world via WCF */


        /// <summary>
        /// Code to run on a new airport 
        /// </summary>
        /// <returns>The airport that the client can service.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public Airport AirportInitialConnect()
        {
            Airport airport = null;
            int apID = 0;

            /* Find the first airport which is not assigned to a slave and 
             * return it while storing the callback object in the dictionary
             * for future use.
             */
            try
            {
                apID = getFirstNullAirportCallback();

                airport = m_airports[apID];
                m_airportCallBacks[apID] = OperationContext.Current.GetCallbackChannel<IATCBizSlaveControllerCallBack>();

                Console.WriteLine(airport.Name + " has connected.");
            }
            catch (InvalidOperationException e)
            {
                // Swallow the exception and return null.
            }
            catch (CommunicationException e)
            {
                // Remove the callback object since it is not valid anymore
                m_airportCallBacks[apID] = null;
            }

            return airport;
        }

        /// <summary>
        /// Disconnect the specified airport updating the master's copy.
        /// </summary>
        /// <param name="airport">The airport to disconnect.</param>
        public void DisconnectAirport(Airport airport)
        {
            Console.WriteLine(airport.Name + " has disconnected.");
            m_airports[airport.ID] = airport;
            m_airportCallBacks[airport.ID] = null;
        }

        /// <summary>
        /// Hand over a plane to another airport to control
        /// </summary>
        /// <param name="plane">The plane transfer control of.</param>
        /// <param name="AirportID">The airport to transfer control to.</param>
        public void HandOverPlane(Plane plane, int AirportID)
        {
            m_airportCallBacks[AirportID].TransferPlane(plane);
        }

        /// <summary>
        /// Perform a time increment of the speicified amount of time.
        /// </summary>
        /// <param name="minutes">The number of minutes to increment by.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void PerformTimeTick(int minutes)
        {
            TimeTickDelegate tickDel;
            Dictionary<TimeTickDelegate, IAsyncResult> delegateToResult = new Dictionary<TimeTickDelegate, IAsyncResult>();

            m_time += minutes;

            // Perform a tick on each slave. Even though the TimeTick method is 
            // OneWay, in bad network conditions, the time the ACK takes to return
            // could be non trivial, hence the Asyncronous calls.
            foreach (IATCBizSlaveControllerCallBack cb in m_airportCallBacks.Values)
            {
                tickDel = cb.TimeTick;
                delegateToResult[tickDel] = tickDel.BeginInvoke(minutes, null, null);
            }

            // Wait for all the slaves to finish before finishing the method.
            foreach (KeyValuePair<TimeTickDelegate, IAsyncResult> delToCB in delegateToResult)
            {
                try
                {
                    delToCB.Key.EndInvoke(delToCB.Value);
                }
                catch (CommunicationException e)
                {
                    throw new InvalidOperationException("Client update failed");
                }
            }
        }

        /// <summary>
        /// Get a copy of the AirRoute given an id.
        /// </summary>
        /// <param name="id">The AirDoute ID</param>
        /// <returns>A copy of the AirRoute</returns>
        public AirRoute GetAirRoute(int id)
        {
            AirRoute route = null;

            foreach (Airport a in m_airports.Values)
            {
                foreach (AirRoute r in a.DepartingAirRoutes.Values)
                {
                    if (r.ID == id)
                    {
                        // Return directly to avoid wasting time searching the 
                        // other airports
                        return r;
                    }
                }
            }

            // Have to return here as well inc case AirRoute not found
            return route;
        }


        /********** PRIVATE METHODS **********/


        /// <summary>
        /// Load the database from the DLL into memory.
        /// </summary>
        private void loadDatabase()
        {
            ATCDB db = new ATCDB();

            m_airports = loadAirports(db);
        }

        /// <summary>
        /// Find the first Callback entry which is null.
        /// </summary>
        /// <returns>The AirportID call back which is null.</returns>
        private int getFirstNullAirportCallback()
        {
            foreach (KeyValuePair<int, IATCBizSlaveControllerCallBack> cb in m_airportCallBacks)
            {
                if (cb.Value == null)
                {
                    // Return immediately if found null callback.
                    return cb.Key;
                }
            }

            throw new InvalidOperationException("No Airports Left to allocate.");
        }

        /// <summary>
        /// Load the Airports from the DLL.
        /// </summary>
        /// <param name="db">The database object to load from</param>
        /// <returns>Map of ID to Airport.</returns>
        private Dictionary<int, Airport> loadAirports(ATCDB db)
        {
            Dictionary<int, Airport> airports = new Dictionary<int, Airport>();
            Airport tempAirport;
            String tempName;

            int[] airportIDs = db.GetAirportIDList();

            foreach (int airportID in airportIDs)
            {
                db.LoadAirport(airportID, out tempName);
                tempAirport = new Airport(airportID, tempName);
                tempAirport.DepartingAirRoutes = loadAirRoutes(db, tempAirport);
                tempAirport.Planes = loadPlanes(db, tempAirport);
                airports[airportID] = tempAirport;
            }

            return airports;
        }

        /// <summary>
        /// Loads the AirRoutes for a given Airport.
        /// </summary>
        /// <param name="db">The database object to load from.</param>
        /// <param name="airport">The airport to get the AirRoutes for</param>
        /// <returns>Map of AirRoute ID to AirRoute.</returns>
        private Dictionary<int, AirRoute> loadAirRoutes(ATCDB db, Airport airport)
        {
            Dictionary<int, AirRoute> routes = new Dictionary<int, AirRoute>();

            int fromAirportID, toAirportID;
            double routeDistance;

            int[] departingRoutes = db.GetDepartingAirRouteIDsForAirport(airport.ID);

            foreach (int routeID in departingRoutes)
            {
                db.LoadAirRoute(routeID, out fromAirportID, out toAirportID, out routeDistance);
                routes[routeID] = new AirRoute(routeID, fromAirportID, toAirportID, routeDistance);
            }

            return routes;
        }

        /// <summary>
        /// Load the planes for a given airport
        /// </summary>
        /// <param name="db">The database object to load from.</param>
        /// <param name="airport">The airport to get the planes for.</param>
        /// <returns>A map of PlaneID to Planes.</returns>
        private Dictionary<int, Plane> loadPlanes(ATCDB db, Airport airport)
        {
            Dictionary<int, Plane> planes = new Dictionary<int, Plane>();

            String planeType;
            double cruisingKPH, fuelConsPerHour;
            int airportID;

            int[] planeIDs = db.GetAirplaneIDsForAirport(airport.ID);

            foreach (int planeID in planeIDs)
            {
                db.LoadAirplane(planeID, out planeType, out cruisingKPH, out fuelConsPerHour, out airportID);
                planes[planeID] = new Plane(planeID, planeType, cruisingKPH, fuelConsPerHour, airportID);
            }

            return planes;
        }
    }
}
