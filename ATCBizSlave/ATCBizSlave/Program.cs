﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCBizMaster;

namespace ATCBizSlave
{
    class Program
    {
        static ATCSlave atcSlave;
        
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += OnProcessExit;

            try
            {
                atcSlave = new ATCSlave();
            }
            catch (ApplicationException e)
            {
                Console.WriteLine("Could not initialise Airport:");
                Console.WriteLine("    " + e.Message);
            }
            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }

        static void OnProcessExit(object sender, EventArgs e)
        {
            atcSlave.Cleanup();
        }
    }
}
