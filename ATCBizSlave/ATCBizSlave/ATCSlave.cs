﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ATCBizMaster;
using System.ServiceModel;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace ATCBizSlave
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class ATCSlave : IATCBizSlaveControllerCallBack
    {
        private ConsoleColor[] colourAlloc = { ConsoleColor.White, ConsoleColor.Cyan, ConsoleColor.Yellow, ConsoleColor.Green };

        private IATCBizSlaveController m_master;
        private Airport m_airport;
        private Queue<AirRoute> m_routeAllocations;
        private int m_time;
        private int m_15minTick;
        private bool m_readyForTakeOff;
        private bool m_readyForLanding;

        /// <summary>
        /// Initialise the ATV Slave.
        /// </summary>
        public ATCSlave()
        {
            connectToServer();
            m_airport = getAirportFromServer();

            if (m_airport == null)
            {
                throw new ApplicationException("No Airports available from server.");
            }
            else
            {
                Console.ForegroundColor = colourAlloc[m_airport.ID - 1];
                Console.WriteLine("This airport is " + m_airport.Name);
            }

            // Create a special queue for deciding which AirRoute is next.
            m_routeAllocations = new Queue<AirRoute>();

            foreach (AirRoute ar in m_airport.DepartingAirRoutes.Values)
            {
                m_routeAllocations.Enqueue(ar);
            }

            m_time = 0;
            m_15minTick = 0;
            m_readyForTakeOff = true;
            m_readyForLanding = true;
        }

        /// <summary>
        /// Get the name of this airport.
        /// </summary>
        /// <returns>The airport name</returns>
        public String GetName()
        {
            return m_airport.Name;
        }

        /// <summary>
        /// Cleanup for when closing down.
        /// </summary>
        public void Cleanup()
        {
            try
            {
                m_master.DisconnectAirport(m_airport);
                ((IClientChannel)m_master).Close();
            }
            catch (CommunicationException e)
            {
                // Swallow and continue 
            }

            m_airport = null;
            m_master = null;
        }


        /********** CALLBACK FUNCTIONS **********/
        /* These methods are exposed to the outside world via WCF */


        /// <summary>
        /// Handle a plane being transferred to this Airport.
        /// </summary>
        /// <param name="plane">The plane being taken control of.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TransferPlane(Plane plane)
        {
            Console.WriteLine(m_airport.Name + ": Taking control of planeID:"
                + plane.ID);

            plane.CurrentAirportID = m_airport.ID;

            double planeRouteDistance = m_master.GetAirRoute(plane.CurrentAirRouteID).DistanceKM;

            // The plane should be <300km away to be transferred, but just double checking.
            if (planeRouteDistance - plane.DistanceAlongRoute < 300.0)
            {
                plane.State = PlaneState.EnteringCircling;
            }

            m_airport.Planes[plane.ID] = plane;
        }

        /// <summary>
        /// Handle a segment of time ticking.
        /// </summary>
        /// <param name="min">The number of minutes that have passed.</param>
        public void TimeTick(int min)
        {
            m_time += min;
            Console.WriteLine("Time ticked at " + m_airport.Name + " by " + min
                + " minutes. It is now t=" + m_time + " (tick " + m_15minTick + ")");

            // When 15min have passed a new plane can land and take off.
            if (m_15minTick < m_time / 15)
            {
                m_15minTick = m_time / 15;
                m_readyForTakeOff = true;
                m_readyForLanding = true;
            }

            PlaneUtils.AddTimeToLandedPlanes(m_airport.Planes.Values.ToList(), min);
            updatePlanesInTransit(min);
            updatePlanesCircling(min);
            takeOffLandedPlane();
        }

        /// <summary>
        /// Return a copy of the airport data held here.
        /// </summary>
        /// <returns></returns>
        public Airport GetThisAirport()
        {
            return m_airport;
        }


        /********** PRIVATE FUNCTIONS **********/


        /// <summary>
        /// Find the plane that has been landed the longest and get it to take off.
        /// </summary>
        /// <param name="min">The amount of time which has elapsed.</param>
        private void takeOffLandedPlane()
        {
            Plane planeToTakeOff;

            // Find the plane that is landed and been waiting the longest.
            List<Plane> planesReadyForTakeOff = PlaneUtils.GetPlanesReadyForTakeOff(m_airport.Planes.Values.ToList());

            if (planesReadyForTakeOff.Count > 0)
            {
                planeToTakeOff = PlaneUtils.LongestLandedPlane(planesReadyForTakeOff);

                if (m_readyForTakeOff && planeToTakeOff != null)
                {
                    takeOffPlane(planeToTakeOff);
                    m_readyForTakeOff = false;
                }
            }


        }

        /// <summary>
        /// Update all planes that are circling and entering circling and land
        /// one if able.
        /// </summary>
        /// <param name="min">The number of minutes that have elapsed.</param>
        private void updatePlanesCircling(int min)
        {
            List<Plane> circlingPlanes = PlaneUtils.GetPlanesInState(m_airport.Planes.Values.ToList(), PlaneState.Circling);
            List<Plane> enteringCirclingPlanes = PlaneUtils.GetPlanesInState(m_airport.Planes.Values.ToList(), PlaneState.EnteringCircling);

            Plane circlingLowestFuel = PlaneUtils.GetPlaneWithLowestFuel(circlingPlanes);
            Plane enteringCirclingLowestFuel = PlaneUtils.GetPlaneWithLowestFuel(enteringCirclingPlanes);

            // Land the plane with the lowest fuel
            if (circlingLowestFuel != null && enteringCirclingLowestFuel != null)
            {
                Plane planeToLand = PlaneUtils.ChoosePlaneToLand(m_master, circlingLowestFuel, enteringCirclingLowestFuel, min);
                landPlane(planeToLand);
            }
            else if (enteringCirclingLowestFuel == null && circlingLowestFuel != null)
            {
                landPlane(circlingLowestFuel);
            }
            else if (circlingLowestFuel == null && enteringCirclingLowestFuel != null)
            {
                if (PlaneUtils.CanEnteringCirclingPlaneReachAirport(m_master, enteringCirclingLowestFuel, min))
                {
                    landPlane(enteringCirclingLowestFuel);
                }
            }

            // Need to update lists as one of the planes may have landed.
            circlingPlanes = PlaneUtils.GetPlanesInState(m_airport.Planes.Values.ToList(), PlaneState.Circling);
            enteringCirclingPlanes = PlaneUtils.GetPlanesInState(m_airport.Planes.Values.ToList(), PlaneState.EnteringCircling);
            
            PlaneUtils.AddTravelDistance(m_master, enteringCirclingPlanes, min);
            PlaneUtils.UpdatePlaneFuelLevels(circlingPlanes, min);
            PlaneUtils.UpdatePlaneFuelLevels(enteringCirclingPlanes, min);

        }

        /// <summary>
        /// Land a plane at this airport
        /// </summary>
        /// <param name="plane">The plane to land.</param>
        private void landPlane(Plane plane)
        {
            if (m_readyForLanding)
            {
                plane.State = PlaneState.Landed;
                plane.DistanceAlongRoute = -1.0;
                plane.TimeLanded = 0;
                plane.CurrentAirRouteID = -1;

                m_readyForLanding = false;

                Console.WriteLine(m_airport.Name + ": PlaneID: " + plane.ID + " landing.");
            }
        }

        /// <summary>
        /// Take off a plane.
        /// </summary>
        /// <param name="plane">The plane to take off.</param>
        private void takeOffPlane(Plane plane)
        {
            if (m_readyForTakeOff)
            {
                plane.State = PlaneState.InTransit;
                plane.DistanceAlongRoute = 0.0;
                plane.TimeLanded = -1;
                plane.CurrentAirportID = -1;

                AirRoute route = getNextRoute();
                plane.CurrentAirRouteID = route.ID;

                // Ideally would refuel as soon as the plane is ready for takeoff 
                // but since routes are only allocated on takeoff, has to be done
                // now.
                double routeHours = route.DistanceKM / plane.CruisingKPH;
                plane.FuelLitres = routeHours * plane.FuelConsPerHour * 1.15;

                Console.WriteLine(m_airport.Name + ": PlaneID: " + plane.ID + " taking off.");
            }
        }

        /// <summary>
        /// Remove the next route from the queue, add to the end again and return it.
        /// </summary>
        /// <returns></returns>
        private AirRoute getNextRoute()
        {
            AirRoute r = m_routeAllocations.Dequeue();
            m_routeAllocations.Enqueue(r);
            return r;
        }

        /// <summary>
        /// Update any planes currently flying. If they get within 300kms to their
        /// destination, hand off to the approriate airport.
        /// </summary>
        /// <param name="min">The minutes that have elapsed.</param>
        private void updatePlanesInTransit(int min)
        {
            List<int> planesToRemove = new List<int>();

            List<Plane> inTransitPlanes = PlaneUtils.GetPlanesInState(m_airport.Planes.Values.ToList(), PlaneState.InTransit);

            foreach (Plane p in inTransitPlanes)
            {
                AirRoute route = m_airport.DepartingAirRoutes[p.CurrentAirRouteID];
                double routeDist = route.DistanceKM;

                p.DistanceAlongRoute += p.CruisingKPH * (double)min / 60.0;

                if (routeDist - p.DistanceAlongRoute < 300.0)
                {
                    planesToRemove.Add(p.ID);
                    m_master.HandOverPlane(p, route.ToAirportID);
                    Console.WriteLine(m_airport.Name + ": Handing over "
                        + "PlaneID " + p.ID + " to APID " + route.ToAirportID);
                }
                else
                {
                    // Only update fuel when plane has not been handed to another
                    // airport since the fuel level will get updated as it
                    // enters circling.
                    PlaneUtils.UpdatePlaneFuelLevels(p, min);

                    // Since in transit, nothing should crash at this stage (when fuel levels
                    // are updated.
                    Debug.Assert(PlaneUtils.GetPlanesInState(inTransitPlanes, PlaneState.Crashed).Count == 0);

                    Console.WriteLine("PlaneID: " + p.ID + " "
                        + p.DistanceAlongRoute + "km along route with "
                        + p.FuelLitres + "L of fuel left.");
                }
            }

            // Remove the planes that have been handed to another airport.
            foreach (int id in planesToRemove)
            {
                m_airport.Planes.Remove(id);
            }
        }

        /// <summary>
        /// Connect to the RPC Server.
        /// </summary>
        private void connectToServer()
        {
            try
            {
                DuplexChannelFactory<IATCBizSlaveController> dataFactory;

                // Connect to the server on local host and increase the max TCP message size
                NetTcpBinding tcpBinding = new NetTcpBinding();
                tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
                tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;
                string sURL = HostLocations.ATCMasterURL;

                dataFactory = new DuplexChannelFactory<IATCBizSlaveController>(this, tcpBinding, sURL);

                m_master = dataFactory.CreateChannel();
            }
            catch (CommunicationException e)
            {
                throw new ApplicationException("Could not connect to Server.");
            }

            if (m_master == null)
            {
                throw new ApplicationException("Could not connect to Server.");
            }
        }

        /// <summary>
        /// Perform the initial connect to the server, initialising the airport
        /// associated with this ATC Slave.
        /// </summary>
        /// <returns></returns>
        private Airport getAirportFromServer()
        {
            Airport a = null;

            try
            {
                a = m_master.AirportInitialConnect();
            }
            catch (CommunicationException e)
            {
                throw new ApplicationException("Could not connect to server.");
            }

            return a;
        }
    }
}
