﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ATCPlaneView.aspx.cs" Inherits="ATCPlaneView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" type="text/css" href="CSS/ATCStyle.css" />
    <title id="txtTitle" runat="server"></title>
</head>
<body>
    <form id="frmPlanes" runat="server" action="ATCWebViewer.aspx">
    <div>
        <h1 id="h1Airport" runat="server"></h1>

        <!-- The planes in the airports -->
        <table id="tblPlanes" runat="server">
        </table>
        <br />
        <input class="button" type="submit" value="Back" />
    </div>
    </form>
</body>
</html>
