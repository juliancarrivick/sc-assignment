﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ATCWebViewer.aspx.cs" Inherits="ATCWebViewer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="Stylesheet" type="text/css" href="CSS/ATCStyle.css" />
    <title>ATC Web Viewer</title>
</head>
<body>
    <h1>ATC Web Viewer</h1>
    <form id="frmATC" runat="server">
    <table>
        <tr>
            <td>Simulation Time:</td>
            <td id="txtime" runat="server">0 minutes</td>
        </tr>
    </table>
    
    <input id="btnTick" class="button" type="button" value="Tick 15 minutes" runat="server" onserverclick="btnTick_ServerClick" />
    <br />
    <div>
        <!-- The airport choices -->
        <div id="divAirports" runat="server">

        </div>
        <input id="btnAirport" class="button" type="button" value="Choose Airport" runat="server" onserverclick="btnAirport_ServerClick" />
    </div>
    <div id="divMsg" runat="server">
    </div>
    </form>
</body>
</html>
