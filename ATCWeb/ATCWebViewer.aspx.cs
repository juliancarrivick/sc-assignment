﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ATCBizMaster;
using System.ServiceModel;

[CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
public partial class ATCWebViewer : System.Web.UI.Page, IATCBizClientCallback
{
    /// <summary>
    /// Loand the page
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        IATCBizClientController master = connectToServer();
        int time = master.InitialConnect();

        txtime.InnerHtml = time + " minutes";

        List<Airport> airports = master.GetAirports().Values.ToList();

        master.Disconnect();

        foreach (Airport a in airports)
        {
            RadioButton r = new RadioButton();
            r.ID = a.ID.ToString();
            r.Text = a.Name;
            r.GroupName = "airports";
            r.Checked = false;
            divAirports.Controls.Add(r);
            divAirports.Controls.Add(new LiteralControl("<br />"));
        }
    }

    /// <summary>
    /// Callback funtion, does nothing in this case.
    /// </summary>
    /// <param name="airports"></param>
    public void UpdateAirports(List<Airport> airports)
    { } // Do nothing, web interface is stateless.

    /// <summary>
    /// Connect to the RPC server.
    /// </summary>
    /// <returns>A controller interface to use.</returns>
    private IATCBizClientController connectToServer()
    {
        IATCBizClientController master = null;
        try
        {
            DuplexChannelFactory<IATCBizClientController> dataFactory;

            // Connect to the server on local host and increase the max TCP message size
            NetTcpBinding tcpBinding = new NetTcpBinding();
            tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
            tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;
            string sURL = HostLocations.ATCClientURL;

            dataFactory = new DuplexChannelFactory<IATCBizClientController>(this, tcpBinding, sURL);

            master = dataFactory.CreateChannel();
        }
        catch (CommunicationException e)
        {
            master = null;
        }

        return master;
    }

    /// <summary>
    /// Code to run when btnAirport is clicked.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAirport_ServerClick(Object sender, EventArgs e)
    {
        int selectedAirportID = getSelectedAirportID();

        if (selectedAirportID == -1)
        {
            divMsg.InnerHtml = "Please select a Airport to view.";
        }
        else
        {
            IATCBizClientController master = connectToServer();
            int time = master.InitialConnect();
            Airport airport = master.GetAirport(selectedAirportID);

            txtime.InnerHtml = time + " minutes";

            master.Disconnect();

            // Add airport data to session data and redirect to the plane view.
            Session["Airport"] = airport;
            Server.Transfer("ATCPlaneView.aspx");
        }
    }

    /// <summary>
    /// Code to run when btnTick is pressed.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnTick_ServerClick(Object sender, EventArgs e)
    {
        IATCBizClientController master = connectToServer();
        int time = master.InitialConnect();

        master.PerformTimeTick(15);
        time += 15;

        txtime.InnerHtml = time + " minutes";

        master.Disconnect();
    }

    /// <summary>
    /// Get the airport that is selected with the radio buttons.
    /// </summary>
    /// <returns>The airport ID that is selected. -1 if no selection</returns>
    private int getSelectedAirportID()
    {
        String airportIDString = "";
        int airportID;

        List<RadioButton> radioButtons = divAirports.Controls.OfType<RadioButton>().ToList();

        foreach (RadioButton r in radioButtons)
        {
            if (r.Checked == true)
            {
                airportIDString = r.ID;
                break; // To avoid keeping on searching.
            }
        }

        try
        {
            airportID = Convert.ToInt32(airportIDString);
        }
        catch (FormatException e)
        {
            airportID = -1;
        }

        return airportID;
    }
}