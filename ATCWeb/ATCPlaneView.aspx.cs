﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ATCBizMaster;

public partial class ATCPlaneView : System.Web.UI.Page
{
    /// <summary>
    /// Code to run on page load.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        Airport airport = (Airport)Session["Airport"];

        if (airport == null)
        {
            Server.Transfer("ATCWebViewer.aspx");
        }
        else
        {
            txtTitle.Text = "ATC " + airport.Name;
            h1Airport.InnerHtml = airport.Name + "'s Planes";

            buildPlaneList(airport);
        }
    }

    /// <summary>
    /// Given an airport, build the table of planes.
    /// </summary>
    /// <param name="airport"></param>
    private void buildPlaneList(Airport airport)
    {
        List<String> properties;

        // Could work these out via Reflection, but that is a bit overkill.
        String[] headers = { "ID", "State", "Type", "Fuel" };

        // Create the headers.
        HtmlTableRow headerRow = new HtmlTableRow();

        foreach (String header in headers)
        {
            HtmlTableCell headerCell = new HtmlTableCell("th");
            headerCell.InnerHtml = header;
            headerRow.Cells.Add(headerCell);
        }

        tblPlanes.Controls.Add(headerRow);

        // Now add each plane entry.
        foreach (Plane p in airport.Planes.Values)
        {
            HtmlTableRow tRow = new HtmlTableRow();

            properties = getPlaneProperties(p);

            foreach (String prop in properties)
            {
                HtmlTableCell tCell = new HtmlTableCell();
                tCell.Align = "Right";
                tCell.InnerHtml = prop;
                tRow.Cells.Add(tCell);
            }

            tblPlanes.Controls.Add(tRow);
        }
    }

    /// <summary>
    /// Get each of the relevent details from a plane
    /// </summary>
    /// <param name="p">The plane</param>
    /// <returns>A list of plane properties to add to the table</returns>
    private List<String> getPlaneProperties(Plane p)
    {
        List<String> props = new List<String>();

        props.Add(p.ID.ToString());
        props.Add(p.State.ToString());
        props.Add(p.Type);

        // Format fuel as float to 2 d.p.
        props.Add(p.FuelLitres.ToString("F2") + "L");

        return props;
    }
}