﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ATCBizMaster;
using System.ServiceModel;
using System.Data;
using System.ComponentModel;

namespace ATCGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public partial class MainWindow : Window, IATCBizClientCallback
    {
        private IATCBizClientController m_master;
        private List<Airport> m_airports;
        private int m_time = 0;

        public MainWindow()
        {
            InitializeComponent();
        }


        /********** GUI Events *********/


        /// <summary>
        /// Code to run on Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void winATC_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                connectToServer();

                m_airports = m_master.GetAirports().Values.ToList();

                dtgAirports.ItemsSource = m_airports;
                dtgAirports.SelectedIndex = 0;
            }
            catch (CommunicationException ex)
            {
                btnTick.IsEnabled = false;
                addToLog("Could not connect to server. Please reconnect and try again.", Brushes.Red);
            }
        }

        /// <summary>
        /// Code to run when application closes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void winATC_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                m_master.Disconnect();
            }
            catch (CommunicationException ex)
            {
                // Do nothing, program is closing down anyway.
            }
        }

        /// <summary>
        /// Code to run when an airport is selected.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtgAirports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Airport a = (Airport)dtgAirports.SelectedItem;

            if (a != null)
            {
                List<Plane> outboundPlanes = PlaneUtils.GetPlanesInState(a.Planes.Values.ToList(), PlaneState.Landed);
                outboundPlanes.AddRange(PlaneUtils.GetPlanesInState(a.Planes.Values.ToList(), PlaneState.InTransit));

                List<Plane> inboundPlanes = PlaneUtils.GetPlanesInState(a.Planes.Values.ToList(), PlaneState.Circling);
                inboundPlanes.AddRange(PlaneUtils.GetPlanesInState(a.Planes.Values.ToList(), PlaneState.Crashed));
                inboundPlanes.AddRange(PlaneUtils.GetPlanesInState(a.Planes.Values.ToList(), PlaneState.EnteringCircling));

                dtgOutbound.ItemsSource = outboundPlanes;
                dtgInbound.ItemsSource = inboundPlanes;
            }
            else
            {
                dtgOutbound.ItemsSource = null;
                dtgInbound.ItemsSource = null;
            }
        }

        /// <summary>
        /// Code to run when the time tick button is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTick_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                m_master.PerformTimeTick(15);
            }
            catch (CommunicationException ex)
            {
                btnTick.IsEnabled = false;
                addToLog("Could not connect to server. Please reconnect and try again.", Brushes.Red);
            }
        }

        /// <summary>
        /// Attempt to reconnect to the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReconnect_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                connectToServer();
                btnTick.IsEnabled = true;
                addToLog("Connected to Server.", Brushes.Green);
            }
            catch (CommunicationException ex)
            {
                addToLog("Could not connect to server.", Brushes.Red);
            }

        }


        /********** Callback Methods *********/


        /// <summary>
        /// Code to run when server updates the airports.
        /// </summary>
        /// <param name="airports"></param>
        public void UpdateAirports(List<Airport> airports)
        {
            // Since this is a callback method and hence be run on a different 
            // thread we have to access via dispatcher...
            this.Dispatcher.Invoke((Action)(() =>
            {
                int selected = dtgAirports.SelectedIndex;

                m_time += 15;
                txtTimeElapsed.Text = m_time + "min";

                m_airports = airports;
                dtgAirports.ItemsSource = m_airports;

                dtgAirports.SelectedIndex = selected;
            }));

        }


        /********** PRIVATE METHODS *********/


        /// <summary>
        /// Attempt to connect to the RPC server.
        /// </summary>
        private void connectToServer()
        {
            try
            {
                DuplexChannelFactory<IATCBizClientController> dataFactory;

                // Connect to the server on local host and increase the max TCP message size
                NetTcpBinding tcpBinding = new NetTcpBinding();
                tcpBinding.MaxReceivedMessageSize = System.Int32.MaxValue;
                tcpBinding.ReaderQuotas.MaxArrayLength = System.Int32.MaxValue;
                string sURL = HostLocations.ATCClientURL;

                dataFactory = new DuplexChannelFactory<IATCBizClientController>(this, tcpBinding, sURL);

                m_master = dataFactory.CreateChannel();
            }
            catch (CommunicationException e)
            {
                ///throw new ApplicationException("Could not connect to Server.");
            }

            if (m_master != null)
            {
                m_time = m_master.InitialConnect();
                txtTimeElapsed.Text = m_time + "min";
            }
        }

        /// <summary>
        /// Add data to log in the specified colour.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="brush"></param>
        private void addToLog(String msg, SolidColorBrush brush)
        {
            TextRange text = new TextRange(txtLog.Document.ContentStart, txtLog.Document.ContentEnd);
            text.Text = msg + "\n";
            text.ApplyPropertyValue(TextElement.ForegroundProperty, brush);
        }
    }
}
